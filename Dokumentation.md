# Validering och Test


## Setup:
Installera [Python 3.8](https://www.python.org/downloads/), [Pycharm](https://www.jetbrains.com/pycharm/download/#section=windows), [Selenium interpreter] för Pycharm och en webdriver.

## Installera Selenium interpreter:
I pycharm kilcka på File>Settings>Project>Python interpreter sedan på plustecknet och sök "selenium" och install package  

## Göra ett test:
Det finns en väldigt bra guide för hur man kan göra ett Selenium test via python [här](https://selenium-python.readthedocs.io/installation.html).
I detta dokument kommer det vara väldigt simplefierat så det rekommenderas att du kollar på guiden.

Förutsatt att du har allting installerat så ska testen göras i Pycharm och i ett nytt Python projekt. I projektet så ska du ha med:
```
import unittest
from selenium import webdriver
from selenium.webdriver.common.keys import Keys


class PythonOrgSearch(unittest.TestCase):

    def setUp(self):
        self.driver = webdriver.Firefox()

    def test_search_in_python_org(self):
        driver = self.driver

        driver.get("https://pontusgranath.gitlab.io/projekt-spa/")

        import re
        src = driver.page_source

        def checkExists(word):
            x = re.search(word, src)
            self.assertNotEqual(x, None)



if __name__ == "__main__":
    unittest.main()
```
Mellan `self.assertNotEqual(x, None)` och `if __name__ == "__main__":` skriver du vad du vill att tester söker, och efter i denhär strukturen `checkExists(r'')` skriver du det du söker efter. Du kan t.ex. skriva `checkExists(r'Öppettider')` för att testet ska söka på hemsidan efter texten som säger "Öppettider".


## Köra ett test:
För att köra ett test behöver du helt enkelt högerklicka på filnamnet inom Pycharm och sedan klicka Run.

## Validera HTML och CSS
Du kan hitta våra tester inom Python mappen i detta repo. **OBS att våra tester använder sig utav Firefox Geckodriver**. För valideringen använder vi w3schools validatorer. 
Html: https://validator.w3.org/ 
CSS: https://jigsaw.w3.org/css-validator/


