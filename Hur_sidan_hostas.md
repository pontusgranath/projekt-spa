# Hur hemsidan är hostad

## Gitlab Pages

Med hjälp utav Gitlab Pages så får våran hemsida en domän och användare har tillgång till den från nätet. Hemsidan använder sig utav ett repository (https://gitlab.com/pontusgranath/projekt-spa.git). Alla filer som är kopplade till hemsidan ska laddas upp till repot.

## .gitlab-ci.ymi

Detta är en config fil för Gitlab som krävs för att hemsidan ska fungera. Utan den så kommer man inte kunna komma åt hemsidan. Denna config släpper ut allt till nätet. Du kan komma åt config filen [här](https://gitlab.com/pontusgranath/projekt-spa/-/blob/master/.gitlab-ci.yml)

## CI / CD

I [CI / CD](https://gitlab.com/pontusgranath/projekt-spa/-/pipelines) kan du se filernas piplelines. Pipelinsen kan berätta för dig om något fungerar eller gick snett. Det kan du lätt förstå om du ser en grön "Passed" ruta eller en röd "Failed" ruta. Hemsidan blir uppdaterad när den gröna ruta uppstår.
